import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WeatherappMainComponent } from './weatherapp-main.component';

describe('WeatherappMainComponent', () => {
  let component: WeatherappMainComponent;
  let fixture: ComponentFixture<WeatherappMainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WeatherappMainComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherappMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
